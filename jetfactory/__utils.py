#!/usr/bin/env python
from requests import get, head
from clint.textui import progress
from os import path

def wget(url, outdir = None):
    ''' File download with progress helper '''
    if outdir is not None:
        fetched = outdir + path.basename(url)
    else:
        fetched = path.basename(url)

    req = head(url, allow_redirects=True)
    size = int(req.headers.get('content-length', -1))

    res = get(url, allow_redirects=True, stream=True)
    if (not path.exists(fetched) \
    and not path.exists(path.splitext(fetched)[0])) \
    or not path.getsize(fetched) == size:
        print("\nDownloading rootfs image: {} \
            of size: {}MiB".format(fetched, size * 1048576))
        with open(fetched, 'wb') as fd:
            for chunk in progress.bar(res.iter_content(chunk_size=1024),
            expected_size=(size/1024) + 1): 
                if chunk:
                    fd.write(chunk)
                    fd.flush()
    return fetched
