# SWR2DISK

## Usage

```
usage: JetFactory swr2disk [-h] [-a [TYPE|PATH] [SIZE]] -d DISK [-e EMPTY [EMPTY ...]] [-l [PATH] [SIZE]] [-n NAND [NAND ...]] [-r [OLD] [NEW]]

optional arguments:
  -h, --help            show this help message and exit

  -a [TYPE|PATH] [SIZE], --android [TYPE|PATH] [SIZE]
                        Flash lineage zip.

  -d DISK, --disk DISK  Path to disk (/dev/<disk>)

  -e EMPTY [EMPTY ...], --empty EMPTY [EMPTY ...]
                        Create new empty partition.

  -l [PATH] [SIZE], --linux [PATH] [SIZE]
                        Flash linux disk image.

  -n NAND [NAND ...], --nand NAND [NAND ...]
                        Restores rawnand.bin to disk.

  -r [OLD] [NEW], --reflash [OLD] [NEW]
                        Reflash an existing partition.
```

## 0 - Pre-requisite

- Original EMMC rawnand, BOOT0 and BOOT1 backup
- Merged linux partition
- Enough space on new EMMC

## 1 - Flashing process

### 1.1 - Pre flash

Install this module:
```sh
git clone https://gitlab.com/switchroot/gnu-linux/jet-factory
sudo pip3 install --user -e jetfactory
```

Edit PATH and add python modules to path in bashrc (or equivalent):
```sh
echo 'export PATH="/root/.local/bin/:$PATH"' >> /root/.bashrc
```

Refresh environment:
```sh
source /root/.bashrc
```

### 1.2 Flashing

While still being logged as root for flashing rawnand + android + linux :
```sh
sudo python3 -m jetfactory swr2disk -d /dev/<new_emmc> -n <path_to_nand> -a <android_build_type> <userdata_size> -l <path_to_merged.img> <size>
```

### 1.3 - Post flash

- Restore BOOT0 and BOOT1 via Hekate to new EMMC
- Copy bootfiles for android and/or linux to SD Card
