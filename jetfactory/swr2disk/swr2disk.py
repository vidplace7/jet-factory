#!/usr/bin/env python
from brotlicffi import decompress
from os import path, stat, makedirs, removedirs, listdir, getcwd
from argparse import ArgumentParser
from stat import S_ISBLK
from jetfactory.__utils import *
from sh import sgdisk, dd, umount, partprobe, blockdev, losetup, mkfs, resize2fs, blkid
import jetfactory.sdat2img.sdat2img as sdat2img
from shutil import rmtree
from zipfile import ZipFile
from glob import glob
from time import sleep
import jetfactory.swr2disk.gpt as gpt
from sys import stdout, stderr

ANDROID_IMAGES = {
    "tab": "https://download.switchroot.org/android-10/icosa-tablet-20210205-rel.zip",
    "tv": "https://download.switchroot.org/android-10/icosa-atv-20210205-rel.zip"
}

class SWR2DISK:
    def __init__(self, disk, nand, android, linux):
        ''' Class constructor '''
        self.android = android
        self.build = getcwd() + "/build/"
        self.basepath = self.build + "switchroot/install/"
        self.disk = disk
        self.linux = linux
        self.nand = nand
        
        # Check if disk is a valid path
        if not S_ISBLK(stat(self.disk).st_mode) or self.disk is None:
            raise FileNotFoundError("Not a valid disk {}.".format(self.disk))

        if self.nand is not None:
            if not path.exists(self.nand[0]):
                raise FileNotFoundError("Invalid path to NAND backup.")

            self._erase()

            # Probe partitions on disk
            partprobe(self.disk)

        # Create build dir
        makedirs(self.basepath, exist_ok=True)

        # Get boot sector type
        self.ptype = blkid("-o", "value", "-s", "PTTYPE", self.disk).strip('\n')

        # Prompt for storage type
        self.storage = input("Select storage type (sd|emmc) : ")

        while self.storage != "sd" or self.storage != "emmc":
            print("Unknown storage type {}".format(self.storage))
            self.storage = input("Select storage type (sd|emmc) : ")

    def __del__(self):
        ''' Class destructor '''
        rmtree(self.build)

    def _erase(self):
        print("WARNING: This will erase everything on {}.".format(self.disk))
        answer = input("Proceed anyway ? (y|n): ")

        if answer != "y":
            print("\nExiting script...")
            exit(0)
        
        # Zap all
        sgdisk("-Z", self.disk)

        # Create new hybrid MBR on SD Card or GPT for eMMC
        if self.storage == "sd":
            sgdisk("-h", self.disk)
        else:
            sgdisk("-og", self.disk)

        # Probe partitions on disk
        partprobe(self.disk)

    def _check_size(self, size):
        ''' Check given size format '''
        if not size.endswith("K") \
            or not size.endswith("M") \
            or not size.endswith("G") \
            or not size != "0":
            raise Exception("\nSize should be expressed in kibibytes (K), \
                mebibytes (M), gibibytes (G) or 0 (take all available space)")

    def _dd(self, src, dst):
        ''' dd wrapper '''
        print("\nRestoring {} to {}".format(src, dst))
        dd(
            "if=" + src,
            "of=" + dst,
            "bs=24M",
            "status=progress",
            "conv=fsync",
            _out=stdout,
            _err=stderr
        )
        print("\nFlash successful !".format(src, dst))

    def  _get_size(self, device):
        ''' Retrieve partition size '''
        return int(blockdev("--getsize64", device)) / int(blockdev("--getss", device))

    def _get_last_partnum(self):
        ''' Get last partition on disk '''
        return len(self._list_partitions(self.disk))

    def _list_partitions(self, device):
        ''' List partitions on device '''
        if device.startswith("/dev/mmc"):
            split = "p"
        elif device.startswith("/dev/loop"):
            split = "p20p"
        else:
            split = device

        return sorted([part for part in glob(device + "*") if part != device], 
            key=lambda name: (int(name.split(split)[1])))

    def _add_part_to_disk(self, src, size = None, name = None):
        ''' Flash partition to disk '''
        if size is None:
            size = str(self. _get_size(src))
        
        print("Creating new empty partition on device.")
        self._new_part(size, name=name)

        print("Writing {} to last usable sector of {}"
            .format(src, self._list_partitions(self.disk)[-1]))

        self._dd(src, self._list_partitions(self.disk)[-1])

        sleep(1)

    def _new_part(self, size = None, fmt = None, name = None):
        ''' Add new partition to disk '''
        # Set partition size
        if size != "0":
            size = "+" + size

        print("Creating new partition on disk {}".format(self.disk))
        sgdisk("--new=0:0:" + size, self.disk)

        if fmt is not None:
            print("Formatting partition as {}".format(fmt))
            mkfs("-t", fmt, self._list_partitions(self.disk)[-1])

        if name is not None:
            print("Applying GPT name {}".format(name))
            sgdisk("-c=0:" + name, self.disk)

        sleep(1)

    def restore_nand(self):
        ''' Restore HOS rawnand.bin from backup '''
        try:
            print("Mount loop rawnand")
            los = str(losetup("--show", "-f", self.nand[0])).strip("\n")
        except:
            print("There was an issue mounting rawnand.bin, unmounting and exiting.")
            losetup("-d", los)
            exit(1)

        print("Probing partitions on loop device")
        partprobe(los)

        print("Flash partitions fetched from loop device to disk")
        for part in self._list_partitions(los):
            self._add_part_to_disk(part)

        print("Restoring old GPT to disk")
        sgdisk("-R=" + self.disk, los)

        print("Unmounting loop device")
        losetup("-d", los)

        # Decrypt using ninfs and resize USER partition
        if len(self.nand) == 3:
            if self.nand[2] != 0 and path.exists(self.nand[1]):
                makedirs("/tmp/USERNAND", exist_ok=True)
                
                mount_nandhac(
                    "--keys",
                    self.nand[1],
                    "--partition",
                    "USER",
                    self._list_partitions()[-1],
                    "/tmp/USERNAND"
                )

                # self._do_resize(self.nand[2])

                umount("/tmp/USERNAND")
                removedirs("/tmp/USERNAND")

    def flash_android(self):
        ''' Flash android to disk '''
        default_parts = [
            [self.build + "vendor.img", "vendor"],
            [self.build + "system.img", "APP"],
            [self.build + "switchroot/install/boot.img", "LNX"],
            [self.build + "switchroot/install/twrp.img", "SOS"],
            [self.build + "switchroot/install/tegra210-icosa.dtb", "DTB"],
            ["MDA", 32768],
            ["CAC", 1433600],
            ["MSC", 6144],
            ["UDA"]
        ]

        if ANDROID_IMAGES[self.android[0]] is not None:
            url = ANDROID_IMAGES[self.android[0]]
        else:
            url = self.android[0]
    
        if url is not None and url.startswith("http") and not path.exists(url):
            wget(url, self.build)

        print("\nFlashing android {}".format(self.android[0]))

        # Extract android zip
        zipf = self.build + path.basename(url)
        with ZipFile(zipf, "r") as zip_ref:
            zip_ref.extractall(self.build)

        zipf = glob(self.build + "lineage-*.zip")[0]
        with ZipFile(zipf, "r") as zip_ref:
            zip_ref.extractall(self.build)

        # Extract vendor and system images
        with open(self.build + "system.new.dat.br", "rb") as system:
            decompress(system.read())
        with open(self.build + "vendor.new.dat.br", "rb") as vendor:
            decompress(vendor.read())

        # Convert system and vendor to img
        sdat2img.main(
            self.build + "system.transfer.list",
            self.build + "system.new.dat.br",
            self.build + "system.img"
        )
        sdat2img.main(
            self.build + "vendor.transfer.list",
            self.build + "vendor.new.dat.br",
            self.build + "vendor.img"
        )

        if self.nand is None and len(self._list_partitions(self.disk)) <= 1:
            self._erase()

        # Flash all android partitions
        for part in default_parts[:5]:
            self._add_part_to_disk(part[0], name=part[1])

        for part in default_parts[5:]:
            # If the partition to be flashed is UDA use given size
            if part[0] == "UDA":
                size = self.android[1]
            else:
                size = part[1]

            fmt = None
            if part[0] == "UDA" or part[0] == "CAC":
                fmt = "ext4"

            self._new_part(size, fmt, part[0])

    def flash_linux(self):
        ''' Flash L4T linux distributions to disk '''
        for distribution in self.linux:

            # Check if disk image exists and is valid
            if not path.exists(distribution[0]) or not distribution[0].endswith(".img"):
                print("\n{} doesn't exist or is invalid.".format(distribution[0]))
                self.linux.pop(distribution)
                self.flash_linux()

            # Flash image
            self._add_part_to_disk(distribution[0], distribution[1])

            # Take all empty space
            resize2fs(self._list_partitions(self.disk)[-1])

def arg_parse(parser):
    ''' Parse arguments '''
    parser.add_argument(
        "-a",
        "--android",
        dest="android",
        metavar=("[TYPE]", "[SIZE]"),
        nargs=2,
        help="Flash lineage zip (tab|tv)."
    )

    parser.add_argument(
        "-d",
        "--disk",
        dest="disk",
        required=True,
        help="Path to disk (/dev/<disk>)"
    )
    
    parser.add_argument(
        "-e",
        "--empty",
        dest="empty",
        action="append",
        # metavar=("[FORMAT]", "[SIZE]", "[LABEL]"),
        nargs="+",
        help="Create new empty partition."
    )

    parser.add_argument(
        "-g",
        "--gpt",
        dest="gpt",
        help="Update GPT header on disk."
    )

    parser.add_argument(
        "-l",
        "--linux",
        dest="linux",
        action="append",
        metavar=("[PATH]", "[SIZE]"),
        nargs=2,
        help="Flash linux disk image (.img)."
    )
    
    parser.add_argument(
        "-n",
        "--nand",
        dest="nand",
        nargs="+",
        help="Restores rawnand.bin to disk."
    )

def main(args):
    # Init SWR2DISK
    swr2disk = SWR2DISK(args.disk, args.nand, args.android, args.linux)

    if swr2disk.ptype == "gpt":
        print("Expending gpt table to 128 partitions.")
        sgdisk(
            "--resize-table=128",
            swr2disk.disk,
            _out=stdout,
            _err=stderr
        )

    if args.gpt is not None:
        gpt.update_gpt(swr2disk.disk)
        exit(0)

    # Flash NAND backup
    if args.nand is not None:
        if len(args.nand) == 3:
            args._check_size(args.nand[2])
        swr2disk.restore_nand()
    
    # Flash Android
    if args.android is not None:
        swr2disk._check_size(args.android[1])
        swr2disk.flash_android()
    
    # Flash Linux
    if args.linux is not None:
        swr2disk._check_size(args.linux[1])
        swr2disk.flash_linux()

    # Create additional partitions
    if args.empty is not None:
        for new in args.empty:
            if len(new) >= 2:
                swr2disk._new_part(*new)

    if swr2disk.ptype == "gpt":
        gpt.update_gpt(swr2disk.disk)

if __name__ == '__main__':
    # Initialise argument parser
    parser = ArgumentParser()
    
    # Add arguments to parser
    arg_parse(parser)
    
    # Parse arguments
    args = parser.parse_args()
  
    # Pass parser to main and call it
    try:
        main(args)
    
    except AttributeError:
        parser.print_help()
        parser.exit()
