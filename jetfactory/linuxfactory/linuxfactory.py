#!/usr/bin/env python
from argparse import ArgumentParser
from contextlib import contextmanager
from datetime import datetime
import json
import fnmatch
from sh import debootstrap, virt_make_fs, zerofree, split, mount, umount
from shutil import rmtree, copy
from jetfactory.__utils import *
from distutils.dir_util import copy_tree
from patoolib import extract_archive, create_archive
from subprocess import Popen
from sys import stdout, stderr
import os
import gi
gi.require_version('Guestfs', '1.0')
from gi.repository import Guestfs

cur_date = datetime.today().strftime('%Y-%m-%d')
image_ext = ['.img', '.qcow2', '.raw']
archive_ext = ['.tar.gz', '.bz2', '.tbz2', '.xz', '.tar', '.tar.xz', '.7z']

def arg_parse(parser):
    ''' Argument parser '''
    parser.add_argument(
        "-b", "--build", dest="build", nargs='+',
        help="-b, --build [DEVICE] [DISTRIBUTION], \
        Build a DISTRIBUTION set in DEVICE json")
        
    parser.add_argument(
        "-c", "--clean", dest="clean",
        action='store_true', help="-c, --clean, Clean cache")

class LinuxFactory:
    def __init__(self, device, distribution):
        ''' Class constructor '''
        # Retrieve device
        self.device = device
        
        # Retrieve parsed name
        self.name = distribution
        
        # Some distributions put guestfs in /usr/share
        if os.path.exists("/usr/share/guestfs/appliance"):
            os.environ['LIBGUESTFS_PATH'] = "/usr/share/guestfs/appliance"

    def __del__(self):
        ''' Class destructor '''
        rmtree(self.distro_dir, ignore_errors=True)

    def _run(self, cmd):
        ''' subprocess.Popen wrapper '''
        out, err = Popen(cmd, universal_newlines=True,
            shell=True, stdout=stdout,stderr=stderr).communicate()
        
        if err is not None:
            raise Exception(err)

        return out

    def _register_binaries(self):
        ''' Register QEMU binaries'''
        self._run("find /proc/sys/fs/binfmt_misc/ -type f -name 'qemu-*' -exec sh -c 'echo -1 > {}' \;")

        for file in os.listdir('/usr/bin/'):
            if fnmatch.fnmatch(file, 'qemu-*-static'):
                static_suffix="--qemu-suffix=-static"
                break
            else:
                static_suffix=""

        self._run("curl -s https://raw.githubusercontent.com/qemu/qemu/master/scripts/qemu-binfmt-conf.sh \
        | bash -s -- -c yes -p yes --qemu-path=/usr/bin " + static_suffix)

    def _extract(self, fd, out):
        ''' Rootfs extract helper '''
        if fd.endswith(tuple(archive_ext)):
            extract_archive(fd, outdir=out)
        
        elif fd.endswith(tuple(image_ext)):
            g = Guestfs.Session()
            g.add_drive_ro(fd)
            g.launch()
            root = g.inspect_os()
            assert(len(root) == 1)
            g.mount(root[0], "/")
            print("\nFound root at {}. Extracting / from disk image {}."
                .format(root[0], fd))
            g.copy_out("/", out)
            g.umount("/")
        
        else:
            raise Exception("\nERROR: Unsupported file format for {}".format(fd))

        print("Extracted {} successfully".format(fd))

    def _prepare(self):
        ''' Create build directories '''
        # Build dir
        self.build_dir = os.getcwd() + "/linux/"

        # Download dir, used to store base image
        self.dl_dir = self.build_dir + "downloadedFiles/"

        # Distro dir, used for chroot and creating distro
        self.distro_dir = self.build_dir + self.device + "-" + self.name + "/"

        # Chroot dir
        self.chroot_dir = self.distro_dir + "." + self.name
        
        # Disk image name
        self.disk_name = self.distro_dir + self.device + "-" + self.name + "-" + \
            cur_date + ".img"

        # Zip name
        self.zip_name = self.build_dir + self.device + "-" + self.name + "-" + \
            cur_date + ".7z"

        # Remove old zip
        if os.path.exists(self.zip_name):
            os.remove(self.zip_name)
        
        # Remove old chroot dir
        if os.path.exists(self.distro_dir):
            rmtree(self.distro_dir)

        # Create the chroot dir
        os.makedirs(self.chroot_dir, exist_ok=True)

        # Create download dir
        os.makedirs(self.dl_dir, exist_ok=True)

        # Create hekate folders
        if self.device == "icosa":
            os.makedirs(self.distro_dir + "/switchroot/install/", exist_ok=True)
            os.makedirs(self.distro_dir + "/switchroot/" + self.name, exist_ok=True)

    def _parse_json(self):
        ''' JSON parser '''
        # Set config dir path
        self.configdir = os.path.dirname(__file__) \
            + "/configs/" + self.device + "/" + self.name + ".json"

        # Initialize script empty array
        self.script = []

        # Load JSON relatively to current script file to retrieve variables
        with open(self.configdir) as js:
            parsed = json.load(js)

        # Error check build method
        if "url" in parsed:
            self.url = parsed["url"]

        elif "debootstrap" in parsed:
            self.debootstrap = parsed["debootstrap"]
        
        else:
            raise Exception("\nNo URL found for {} : {} configuration."
            .format(self.device, self.name))


        # Error check chroot script
        if "script" not in parsed:
            print("\nNo chroot script found for {} : {} configuration."
            .format(self.device, self.name))
            parsed["script"] = ""
        
        # Pre run script
        if "pre" in parsed:
            for i in range(len(parsed["pre"])):
                config = os.path.dirname(__file__) \
                    + "/configs/" + self.device + "/common/" + parsed["pre"][i] + ".json"
                
                with open(config) as js:
                    self.script += json.load(js)["script"]
        
        # Chroot script
        self.script += parsed["script"]

        # Post run script
        if "post" in parsed:
            for i in range(len(parsed["post"])):
                config = os.path.dirname(__file__) \
                    + "/configs/" + self.device + "/common/" + parsed["post"][i] + ".json"

                with open(config) as js:
                    self.script += json.load(js)["script"]

        # Cache dir
        if "cache" in parsed:
            self.cache = parsed["cache"]
            self.cachedir = self.build_dir + ".cache" + self.cache
            os.makedirs(self.cachedir, exist_ok=True)

    def _extract_rootfs(self, rootfs):
        ''' Image or archive extraction helper '''
        extracted_image = self.dl_dir + os.path.basename(os.path.splitext(rootfs)[0])

        for arch in archive_ext:
            for img in image_ext:
                if rootfs.endswith(img + arch):
                    print("\nFound compressed disk image {}".format(rootfs))
                    if not os.path.exists(extracted_image):
                        self._extract(rootfs, self.dl_dir)
                    self._extract(extracted_image, self.chroot_dir)
                    return

        if os.path.exists(extracted_image):
            self._extract(extracted_image, self.chroot_dir)

        else:
            self._extract(rootfs, self.chroot_dir)

    @contextmanager
    def _chroot_wrapper(self, root):
        # Keep a reference to real root
        real_root = os.open("/", os.O_RDONLY)

        print("\nChrooting in {}\n".format(root))
        try:
            # Unlink resolv.conf if it is a symlink
            if os.path.islink(root + "/etc/resolv.conf"):
                os.unlink(root + "/etc/resolv.conf")

            # Copy host resolv.conf
            copy("/etc/resolv.conf", root + "/etc/resolv.conf")

            # Mount the needed dirs for chroot
            for mnt in ('/proc', '/dev', '/sys'):
                chroot_mnt = root + mnt
                if not os.path.ismount(chroot_mnt):
                    if not os.path.isdir(chroot_mnt):
                        os.makedirs(chroot_mnt)
                    mount(mnt, chroot_mnt, "--rbind")

            # Mount cache separately
            if hasattr(self, "cache"):
                os.makedirs(root + self.cache, exist_ok=True)
                mount(self.cachedir, root + self.cache, "--rbind")

            # Mount hekate boot dir to /boot/switchroot
            if self.device == "icosa":
                os.makedirs(root + "/boot/switchroot", exist_ok=True)
                mount(self.distro_dir + "/switchroot/" + self.name, root + "/boot/switchroot", "--rbind")

            # Set GID 0 = root
            os.setgid(0)
            
            # Set UID 0 = root
            os.setuid(0)
            
            # Chroot to new root
            os.chroot(root)
            
            # Chdir to /
            os.chdir('/')
            
            # Block until all commands are done
            yield

        except:
            # Return to real root
            os.fchdir(real_root)
            
            # Return to current dir
            os.chroot(".")
            
            # Close real root fd
            os.close(real_root)

            for mnt in ('/proc', '/dev', '/sys'):
                chroot_mnt = root + mnt
                if os.path.ismount(chroot_mnt):
                    print("Unmounting {}".format(chroot_mnt))
                    umount("-lf", chroot_mnt)

        finally:
            # Return to real root
            os.fchdir(real_root)
            
            # Return to current dir
            os.chroot(".")
            
            # Close real root fd
            os.close(real_root)

            for mnt in ('/proc', '/dev', '/sys'):
                chroot_mnt = root + mnt
                if os.path.ismount(chroot_mnt):
                    print("Unmounting {}".format(chroot_mnt))
                    umount("-lf", chroot_mnt)

        print("\nChroot install successful !")

    def create_hekate_zip(self):
        ''' Create a 7z archive containing a splitted image file to fit on fat32 '''
        # Check if image needs alignement
        size = os.path.getsize(self.disk_name)

        aligned_size = (size + (4194304-1)) & ~(4194304-1)

        aligned_check = aligned_size - size

        if aligned_check != 0:
            print("\nAligning by adding {} bytes: ".format(str(aligned_check)))
            self._run("dd if=/dev/zero bs=1 count=" + str(aligned_check) + " >> " + self.disk_name)

        print("\nSpliting {} into chunks".format(self.disk_name))
        split("-b4290772992", "--numeric-suffixes=0", self.disk_name, self.distro_dir + "/switchroot/install/" + "l4t.")

        print("Creating hekate compatible 7zip")
        create_archive(self.zip_name, [self.distro_dir + "/switchroot/"])

    def create_distribution(self):
        ''' Create distribution disk image '''
        print("Registering binfmt_misc binaries needed for chroot")
        self._register_binaries()

        print("Preparing build environment")
        self._prepare()

        print("Parsing JSON template")
        self._parse_json()

        # Download and extract base rootfs or debootstrap
        if not hasattr(self, "debootstrap"):
            path = wget(self.url, self.dl_dir)
            self._extract_rootfs(path)

        else:
            print("\nDebootstraping {} for {}. This will take a while..."
                .format(self.name, self.debootstrap[0]))

            debootstrap(
                "--include=wget,gnupg",
                "--arch=" + self.debootstrap[0],
                self.name,
                self.chroot_dir,
                self.debootstrap[1]
            )
        
        # Actuall chroot process
        with self._chroot_wrapper(self.chroot_dir):
            for cmd in self.script:
                self._run(cmd)

        print("\nCreating disk image {}. This will take a while."
            .format(self.disk_name))
        
        # Call virt make fs to create disk image
        virt_make_fs(self.chroot_dir, self.disk_name, "--size=+768M", "-t", \
            "ext4", "--label", "SWR-" + self.name[0:3].upper())

        print("\nRunning zerofree on disk image")
        zerofree(self.disk_name)

        # Pack for different boards
        if self.device == "icosa":
            self.create_hekate_zip()

        else:
            create_archive(self.zip_name, [self.disk_name])

        print("\nDistribution creation done ! You can find the file in {}"
            .format(self.zip_name))

def main(args):
    ''' Entrypoint '''
    if args.clean is True:
        if os.path.exists(os.getcwd() + "/linux/" + ".cache"):
            print("Cleaning cache directory")
            os.removedirs(os.getcwd() + "/linux/" + ".cache")

    if args.build is not None:
        LinuxFactory(
            args.build[0],
            args.build[1]
        ).create_distribution()

if __name__ == '__main__':
    # Initialise argument parser
    parser = ArgumentParser()
    
    # Add arguments to parser
    arg_parse(parser)
    
    # Parse arguments
    args = parser.parse_args()
    
    # Pass parser to main and call it
    try:
        main(args)
    except AttributeError:
        parser.print_help()
        parser.exit()
