#!/usr/bin/env python
import argparse
from jetfactory.swr2disk import swr2disk
from jetfactory.linuxfactory import linuxfactory

if __name__ == '__main__':
    # Create argument parser
    parser = argparse.ArgumentParser(prog="JetFactory")
    commands = parser.add_subparsers(title='sub-commands', dest="commands")

    lnx_parser = commands.add_parser(
        "linuxfactory",
        help="linuxfactory, Build a linux distribution")
    linuxfactory.arg_parse(lnx_parser)
    lnx_parser.set_defaults(func=linuxfactory.main)

    swr_parser = commands.add_parser(
        "swr2disk",
        help="swr2disk, Flash linux, android or restore NAND")
    swr2disk.arg_parse(swr_parser)
    swr_parser.set_defaults(func=swr2disk.main)

    args = parser.parse_args()
    args.func(args)
