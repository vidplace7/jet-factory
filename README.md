# Jet Factory

Create live and flashable linux distribution root filesystem images.

## Dependencies

On ubuntu 18.04:

```sh
sudo apt-get install qemu qemu-user-static binfmt-support unrar xz-utils unzip p7zip-full debootstrap python3 python3-pip libguestfs-tools libguestfs-dev libguestfs-gobject-1.0-0 libguestfs-gobject-dev zerofree git repo ccache
```

```sh
git clone -b python-refactor-wip https://gitlab.com/switchroot/gnu-linux/jet-factory
cd jet-factory
sudo pip install --user -e .
```

## Usage

```txt
usage: sudo python3 -m jetfactory [-h] {linuxfactory,swr2disk} ...

optional arguments:
  -h, --help            show this help message and exit

sub-commands:
  {linuxfactory,swr2disk}
    linuxfactory        linuxfactory, Build a linux distribution
    swr2disk            swr2disk, Flash linux, android or restore NAND
```

### Docker example

```sh
sudo docker run --privileged --rm -it -v "$PWD"/linux:/build/linux registry.gitlab.com/switchroot/gnu-linux/jet-factory:python-refactor-wip linuxfactory -b icosa arch
```

### Building Docker container (optional)

```sh
docker build . -t registry.gitlab.com/switchroot/gnu-linux/jet-factory:python-refactor-wip
```

### Docker tips

*You can override the workdir used in the docker, to use your own changes, without rebuilding the image by adding this repository directory as a volume to the docker command above.*

```sh
-v $(pwd):/build
```

## Credits

### Special mentions

@gavin_darkglider, @CTCaer, @ByLaws, @ave and all the L4S & [switchroot](https://switchroot.org) members \
For their various work and contributions to switchroot.

### Contributors

@Stary2001, @Kitsumi, @parkerlreed, @AD2076, @PabloZaiden, @andrebraga1 \
@nicman23 @Matthew-Beckett @Nautilus @3l_H4ck3r_C0mf0r7
For their work, support and direct contribution to this project.
